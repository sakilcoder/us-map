function onEachState(feature, layer) {

    var popup = L.popup();
    popup.setContent(feature.properties.STATE_NAME);
    layer.bindPopup(popup, popupOptions);

    layer.on('mouseover', function (e) {
        var popup = e.target.getPopup();
        popup.setLatLng(e.latlng).openOn(map);
        this.setStyle({
            'fillColor': '#FF6B6B'
        });
    });

    layer.on('mouseout', function(e) {
        e.target.closePopup();
        this.setStyle({
            'fillColor': '#009DAE'
        });
    });

    layer.on('mousemove', function (e) {
        popup.setLatLng(e.latlng).openOn(map);
    });

    layer.on('click', function (event) {
        if(us_countyLayer){
            map.removeLayer(us_countyLayer);
        }
        tempcityLayer = L.geoJSON(us_cities, {filter: cityFilter, style: styleZipTemp}).addTo(map);
        us_countyLayer = L.geoJson(us_counties, {
            filter: countyFilter, 
            style: styleCounty, 
            onEachFeature: onEachCounty
        }).addTo(map);
        map.fitBounds(us_countyLayer.getBounds());
        map.removeLayer(stateLayer)

        function cityFilter(featureCity) {
            if (feature.properties.STATE_NAME === featureCity.properties.STATE_NAME) return true
        }

        function countyFilter(featureCounty) {
            if (featureCounty.properties.STATE_NAME === feature.properties.STATE_NAME) return true
        }

    });
    
}
function onEachCounty(feature, layer) {


    var popup = L.popup();
    popup.setContent(feature.properties.NAME);
    layer.bindPopup(popup, popupOptions);

    layer.on('mouseover', function (e) {
        var popup = e.target.getPopup();
        popup.setLatLng(e.latlng).openOn(map);
        this.setStyle({
            'fillColor': '#FFD93D'
        });
    });

    layer.on('mouseout', function(e) {
        e.target.closePopup();
        this.setStyle({
            'fillColor': '#FD5D5D'
        });
    });

    layer.on('mousemove', function (e) {
        popup.setLatLng(e.latlng).openOn(map);
    });

    layer.on('click', function (event) {
        if(tempcityLayer){
            cityCount = L.geoJson(us_cities, {filter: cityFilter}).getLayers().length;
            if(cityCount>0){
                map.removeLayer(tempcityLayer);
            }
        }
        if(us_cityLayer){
            map.removeLayer(us_cityLayer);
        }
        
        us_cityLayer = L.geoJson(us_cities, {filter: cityFilter, style: styleCity, onEachFeature: onEachCity}).addTo(map);
        if(us_cityLayer.getLayers().length>0){
            map.fitBounds(us_cityLayer.getBounds());
            map.removeLayer(us_countyLayer)
        }

        function cityFilter(featureCity) {
            if (feature.properties.FIPS === featureCity.properties.FIPS) return true
        }

    });
}

function onEachCity(feature, layer) {
    
    var popup = L.popup();
    popup.setContent(feature.properties.NAME);
    layer.bindPopup(popup, popupOptions);

    layer.on('mouseover', function (e) {
        var popup = e.target.getPopup();
        popup.setLatLng(e.latlng).openOn(map);
        this.setStyle({
            'fillColor': '#FFD93D'
        });
    });

    layer.on('mouseout', function(e) {
        e.target.closePopup();
        this.setStyle({
            'fillColor': '#00B4D8'
        });
    });

    layer.on('mousemove', function (e) {
        popup.setLatLng(e.latlng).openOn(map);
    });

    layer.on('click', function (event) {
        if(us_zipLayer){
            map.removeLayer(us_zipLayer);
        }
        us_zipLayer = L.geoJson(us_zips, {filter: zipFilter, style: styleZip, onEachFeature: onEachZip}).addTo(map);
        if(us_zipLayer.getLayers().length>0){
            map.fitBounds(us_zipLayer.getBounds());
            map.removeLayer(us_cityLayer)
        }

        function zipFilter(featureZip) {
            console.log(featureZip.properties.GEOID10);
            if (feature.properties.AFFGEOID10 === featureZip.properties.AFFGEOID10) return true
        }

    });
}

function onEachZip(feature, layer) {
    
    var popup = L.popup();
    popup.setContent(feature.properties.GEOID10);
    layer.bindPopup(popup, popupOptions);

    layer.on('mouseover', function (e) {
        var popup = e.target.getPopup();
        popup.setLatLng(e.latlng).openOn(map);
        this.setStyle({
            'fillColor': '#FFD93D'
        });
    });
    layer.on('mouseout', function(e) {
        e.target.closePopup();
        this.setStyle({
            'fillColor': '#00C897'
        });
    });

    layer.on('mousemove', function (e) {
        popup.setLatLng(e.latlng).openOn(map);
    });
    
}
