
var map = L.map('map').setView([50.0, -124.3], 3);
var us_countyLayer;
var us_cityLayer;
var us_zipLayer;
var tempcityLayer;

// var tempcityLayer = L.geoJSON(us_cities, {style: styleZipTemp}).addTo(map);

var stateLayer = L.geoJSON(us_states, {
    style: styleState,
    onEachFeature: onEachState,
}).addTo(map);
    
L.easyButton( 'fa-home fa-lg', function(){
    map.setView([50.0, -124.3], 3);

    if(stateLayer){
        map.removeLayer(stateLayer);
    }
    stateLayer = L.geoJSON(us_states, {
        style: styleState,
        onEachFeature: onEachState,
    }).addTo(map);
    if(us_countyLayer){
        map.removeLayer(us_countyLayer);
    }
    if(us_cityLayer){
        map.removeLayer(us_cityLayer);
    }
    if(us_zipLayer){
        map.removeLayer(us_zipLayer);
    }
    if(tempcityLayer){
        map.removeLayer(tempcityLayer);
    }

  }).addTo(map)
